Pod::Spec.new do |s|
  s.name            = "Ready4AirCore"
  s.homepage         = "http://valtech.com"
  s.license          = 'Commercial'
  s.author           = { "Valtech" => "info@valtech.com" }
  s.summary          = "Ready4Air Core Component"
  s.version         = "0.0.1"
  s.source          = { :http => "https://api.bitbucket.org/1.0/repositories/tapowtv/ready4air-app-libs/raw/master/Ready4Air_Core_Lib-0.0.1.zip" }
  s.preserve_paths  = 'libReady4AirCore.a'
  s.source_files    = '*.h'
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"${PODS_ROOT}/Ready4AirCore"' }
  
  s.dependency 'AFNetworking', '3.0.4'
  s.dependency 'UICKeyChainStore', '2.0.6'
  s.dependency 'Mantle', '2.0.2'
  s.dependency 'Fabric', '1.5.1'
  s.dependency 'Crashlytics', '3.3.1'
  s.dependency 'Typhoon'

end
