    Pod::Spec.new do |s|
    s.name            = "Ready4AirCore"
    s.homepage        = "http://valtech.com"
    s.license         = 'Commercial'
    s.authors         = { "Javier Tarazaga" => "javier.tarazaga@valtech.se", "Ling Yang" => "ling.yang@valtech.se", "Johan Risch" => "johan.risch@valtech.se", "Ostap Kravchuk" => "ostap.kravchuk@valtech.se" }
    s.summary         = "Ready4Air Core Component"
    s.version         = "2.2.3.10"
    s.source          = { :http => "https://api.bitbucket.org/1.0/repositories/tapowtv/ready4air-app-libs/raw/master/Ready4Air_Core_Lib-2.2.3.10.zip" }
    s.preserve_paths  = 'libReady4AirCore.a'
    s.source_files    = '*.h'
    s.platform     = :ios, '7.0'
    s.requires_arc = true
    s.libraries = 'xml2', 'Ready4AirCore'
    s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"${PODS_ROOT}/Ready4AirCore"', 'HEADER_SEARCH_PATHS' => '"$(SDKROOT)/usr/include/libxml2"' }

    s.dependency 'AFNetworking', '3.0.4'
    s.dependency 'UICKeyChainStore', '2.1.0'
    s.dependency 'Mantle', '2.0.2'
    s.dependency 'Typhoon', '3.4.5'
    end
