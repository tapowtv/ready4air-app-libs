    Pod::Spec.new do |s|
    s.name            = "Ready4AirPlayer"
    s.homepage        = "http://valtech.com"
    s.license         = 'Commercial'
    s.authors         = { "Javier Tarazaga" => "javier.tarazaga@valtech.se", "Ling Yang" => "ling.yang@valtech.se", "Johan Risch" => "johan.risch@valtech.se", "Ostap Kravchuk" => "ostap.kravchuk@valtech.se" }
    s.summary         = "Ready4Air Player Component"
    s.version         = "2.1.1"
    s.source          = { :http => "https://api.bitbucket.org/1.0/repositories/tapowtv/ready4air-app-libs/raw/master/player-2.1.1.zip" }
    s.preserve_paths  = 'libReady4AirPlayer.a'
    s.source_files    = '*.h'
    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"${PODS_ROOT}/Ready4AirPlayer"' }

    s.frameworks = 'AVFoundation', 'CoreMedia', 'AudioToolbox', 'MediaPlayer', 'AVKit'
    s.libraries = 'Ready4AirPlayer'

    s.dependency 'AFNetworking', '3.0.4'
    s.dependency 'MSPlayReady', '2.3.2000'
    s.dependency 'SDWebImage', '3.7.5'
    s.dependency 'google-cast-sdk', '2.10.1'
    end
