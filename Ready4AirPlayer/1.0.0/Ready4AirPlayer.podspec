Pod::Spec.new do |s|
  s.name            = "Ready4AirPlayer"
  s.homepage         = "http://valtech.com"
  s.license          = 'Commercial'
  s.author           = { "Jeffrey Thompson" => "jeffrey.thompson@neonstingray.com" }
  s.summary          = "Ready4Air Player Component"
  s.version         = "1.0.0"
  s.source          = { :http => "https://api.bitbucket.org/1.0/repositories/tapowtv/ready4air-app-libs/raw/master/player-1.0.0.zip" }
  s.preserve_paths  = 'libReady4AirPlayer.a'
  s.source_files    = '*.h'
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"${PODS_ROOT}/Ready4AirPlayer"' }
  
  s.dependency 'AFNetworking', '2.6.0'
  s.dependency 'MSPlayReady', '2.3.2000'
  s.dependency 'google-cast-sdk', '2.5.2'
  s.dependency 'SDWebImage'
  s.dependency 'Typhoon'
end
