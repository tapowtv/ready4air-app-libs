#!/usr/bin/python

import sys
import os.path
import subprocess
import xml.etree.ElementTree
import commands



print 'processing: \n'

e = xml.etree.ElementTree.parse('dependencies.xml').getroot()
for dependency in e.findall('dependency'):
    groupId = dependency.find('groupId').text
    artifactId = dependency.find('artifactId').text
    version = dependency.find('version').text
    filename = dependency.find('filename').text
    packaging = dependency.find('packaging').text
    command = "mvn install:install-file -DgroupId=" + groupId + " -DartifactId=" + artifactId + " -Dversion=" + version + " -Dfile=./libs/" + filename + " -Dpackaging=" + packaging + " -DgeneratePom=true -DlocalRepositoryPath=./repository -DcreateChecksum=true"
    print command
    subprocess.check_call(command, shell=True)


print 'processing end'